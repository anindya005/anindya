package com.anindya.spring.io.vo;

public class TestVo2{
	private int integer1;
	private int integer2;
	private int integer3;
	private String str;

	public int getInteger1() {
		return integer1;
	}

	public void setInteger1(int integer1) {
		this.integer1 = integer1;
	}

	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}

}
