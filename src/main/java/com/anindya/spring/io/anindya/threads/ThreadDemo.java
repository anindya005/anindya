package com.anindya.spring.io.anindya.threads;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class ThreadDemo {
public static String callMyAPI() {
	String responsePost="";
	String responseComment="";
	Gson gson=new Gson();
	long myapistartTime = System.nanoTime();
	ExecutorService executorService=Executors.newCachedThreadPool();
	Future<String> outputPost = executorService.submit(new Callable<String>() {

		@Override
		public String call() throws Exception {
					long startTime = System.nanoTime();
					System.out.println("Start Call for Post ::at"+startTime);
					NetClientGet clientGet=new NetClientGet();
					String response=clientGet.callPosts();
					long endTime   = System.nanoTime();
					System.out.println("Ending Call for Post ::"+endTime);
					long totalTime = endTime - startTime;
					System.out.println("Time taken for POST API :: "+TimeUnit.NANOSECONDS.toSeconds(totalTime));
					return response;
				}
	});
	Future<String> outputComment = executorService.submit(new Callable<String>() {

		@Override
		public String call() throws Exception {
					long startTime = System.nanoTime();
					System.out.println("Start Call for Comment ::at"+startTime);
					NetClientGet clientGet=new NetClientGet();
					String response=clientGet.callComments();
					long endTime   = System.nanoTime();
					System.out.println("Ending Call for Comment ::"+endTime);
					long totalTime = endTime - startTime;
					System.out.println("Time taken for Comment API :: "+TimeUnit.NANOSECONDS.toSeconds(totalTime));
					return response;
				}
	});
	executorService.shutdown();
	try {
		responsePost=outputPost.get();
		
		Type collectionTypePost = new TypeToken<Collection<Post>>(){}.getType();
		List<Post> posts = gson.fromJson(responsePost, collectionTypePost);
		
		responseComment=outputComment.get();
		Type collectionTypeComment = new TypeToken<Collection<Comment>>(){}.getType();
		List<Post> comments = gson.fromJson(responsePost, collectionTypeComment);
		System.out.println("Time taken for MY API :: "+TimeUnit.NANOSECONDS.toSeconds(System.nanoTime()-myapistartTime));
		//Posts posts=gson.fromJson(response, Posts.class);
		//posts.forEach(post->System.out.println(post));
	} catch (Exception e) {
		e.printStackTrace();
	} 
	return responsePost;//+gson.toJson(responseComment);
}
}
